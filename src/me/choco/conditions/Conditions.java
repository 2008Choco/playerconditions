package me.choco.conditions;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import me.choco.conditions.utils.Loops;
import me.choco.conditions.utils.Metrics;
import me.choco.conditions.utils.Startup;

public class Conditions extends JavaPlugin implements Listener{
	
	FileConfiguration config;
	File cfile;
	
	public static int frostbiteChanceInt = 20;
	
	@Override
	public void onEnable(){
		getLogger().info("Particle API designed by: DarkBlade12. Big thanks to him");
		
		getLogger().info("Enabling events and command executors");
		Startup.registerEvents();
		Startup.registerCommands();
		
		this.config = getConfig();
		saveDefaultConfig();
		this.cfile = new File(getDataFolder(), "config.yml");
		
		getLogger().info("Creating recipe items and recipe formations");
		Startup.loadItems();
		
		getLogger().info("Enabling condition loops");
		Loops.bleedingLoop();
		Loops.infectionLoop();
		Loops.thalassophobiaLoop();
		Loops.hungerLoop();
		Loops.frostbiteLoop();
		Loops.malariaLoop();
		if (getConfig().getBoolean("Vertigo.VertigoEnabled") == true){
			Loops.vertigoLoop();
		}//Close if vertigo is enabled in config
		if (getConfig().getBoolean("Mystophobia.MystophobiaEnabled") == true){
			Loops.mysticLoop();
		}//Close if mystophobia is enabled in config
		
		if (getConfig().getBoolean("Metrics.MetricsEnabled") == true){
			getLogger().info("Enabling Plugin Metrics");
		    try{
		        Metrics metrics = new Metrics(this);
		        metrics.start();
		    }//Close an attempt to start metrics
		    catch (IOException e){
		    	e.printStackTrace();
		        getLogger().warning("Could not enable Plugin Metrics. If issues continue, please put in a ticket on the "
		        	+ "Player Conditions development page");
		    }//Close if an IOException occurs
		}//Close if pluginmetrics is enabled in config
	}//Close onEnable method
}//Close class

/* TODO LIST! IF I GET IDEAS, WRITE THEM HERE TO ADD LATER:
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * When zombies are wielding a weapon, increase the chance of an infection
 * When mystophobic, have a chance of sending the player obfuscated purple text to spook them
 */