package me.choco.conditions.events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import me.choco.conditions.api.Condition;
import me.choco.conditions.api.PlayerStatus;
import me.choco.conditions.particles.ParticleEffect;
import me.choco.conditions.utils.Messages;

public class PlayerClickEvent implements Listener{
	
	static Plugin PC = Bukkit.getServer().getPluginManager().getPlugin("PlayerConditions");
	static FileConfiguration config = PC.getConfig();
	
	@EventHandler
	public void onPlayerClick(PlayerInteractEvent event){
		Player player = event.getPlayer();
		if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){
			if (itemEquals(player, Material.PAPER, ChatColor.RED + "Bandage")){
				if (PlayerStatus.isBleeding(player)){
					if (player.getHealth() <= 16){
						bandageHandler(player, 4, true);
					}//Close if player's health is less than or equal to 16
					else if (player.getHealth() == 17){
						bandageHandler(player, 3, true);
					}//Close if player's health is 17
					else if (player.getHealth() == 18){
						bandageHandler(player, 2, true);
					}//Close if player's health is 18
					else if (player.getHealth() == 19){
						bandageHandler(player, 1, true);
					}//Close if player's health is 19
					else if (player.getHealth() == 20){
						bandageHandler(player, 0, true);
					}//Close if player's health is 20
				}//Close if player is bleeding
				else{
					if (player.getHealth() <= 16){
						bandageHandler(player, 4, false);
					}//Close if player's health is less than or equal to 16
					else if (player.getHealth() == 17){
						bandageHandler(player, 3, false);
					}//Close if player's health is 17
					else if (player.getHealth() == 18){
						bandageHandler(player, 2, false);
					}//Close if player's health is 18
					else if (player.getHealth() == 19){
						bandageHandler(player, 1, false);
					}//Close if player's health is 19
					else if (player.getHealth() == 20){
						Messages.send(player, ChatColor.DARK_RED, config.getString("Messages.Bleeding.BandageCantUse"));
					}//Close if player's health is 20
				}//Close if player is not bleeding
			}//Close if held item is a "bandage"
			
			if (itemEquals(player, Material.GHAST_TEAR, ChatColor.GREEN + "Infection Cure")){
				if (PlayerStatus.isInfected(player)){
					PlayerStatus.removeCondition(player, Condition.INFECTION);
					Messages.send(player, ChatColor.DARK_GREEN, config.getString("Messages.ZombieInfection.CureWorked"));
					removeCurrentItem(player);
					ParticleEffect.VILLAGER_HAPPY.display((float) 0.5, (float)0.55, (float)0.1, (float)0.1, 25, player.getLocation().add(0, 1, 0), player);
				}//Close if player is currently infected
				else{
					Messages.send(player, ChatColor.DARK_GREEN, config.getString("Messages.ZombieInfection.CureNotInfected"));
					event.setCancelled(true);
				}//Close if player is not infected
			}//Close if held item is a "Infection Cure"
			
			if (itemEquals(player, Material.RED_ROSE, ChatColor.LIGHT_PURPLE + "Flower of Purity")){
				if (PlayerStatus.isMystified(player)){
					PlayerStatus.removeCondition(player, Condition.MYSTOPHOBIA);
					Messages.send(player, ChatColor.DARK_GRAY, config.getString("Messages.Mystophobia.CureWorked"));
					removeCurrentItem(player);
					ParticleEffect.VILLAGER_HAPPY.display((float) 0.5, (float)0.55, (float)0.1, (float)0.1, 25, player.getLocation().add(0, 1, 0), player);
				}//Close if player is currently mystified
				else{
					Messages.send(player, ChatColor.DARK_GRAY, config.getString("Messages.Mystophobia.CureNotMystified"));
					event.setCancelled(true);
				}//Close if player is not mystified
			}//Close if held item is a "Flower of Purity"
			
			if (itemEquals(player, Material.WEB, ChatColor.DARK_GRAY + "I promise. It helps")){
				if (PlayerStatus.isArachnophobic(player)){
					PlayerStatus.removeCondition(player, Condition.ARACHNOPHOBIA);
					Messages.send(player, ChatColor.DARK_GRAY, config.getString("Messages.Arachnophobia.CureWorked"));
					removeCurrentItem(player);
					ParticleEffect.VILLAGER_HAPPY.display((float) 0.5, (float)0.55, (float)0.1, (float)0.1, 25, player.getLocation().add(0, 1, 0), player);
				}//Close if player is currently arachnophobic
				else{
					Messages.send(player, ChatColor.DARK_GRAY, config.getString("Messages.Arachnophobia.CureNotArachnophobic"));
					event.setCancelled(true);
				}//Close if player is not arachnophobic
			}//Close if held item is a "Web"
			
			if (itemEquals(player, Material.EYE_OF_ENDER, ChatColor.DARK_PURPLE + "Bug Spray")){
				event.setCancelled(true);
				if (PlayerStatus.hasMalaria(player)){
					PlayerStatus.removeCondition(player, Condition.MALARIA);
					Messages.send(player, ChatColor.DARK_PURPLE, config.getString("Messages.Malaria.CureWorked"));
					removeCurrentItem(player);
					ParticleEffect.VILLAGER_HAPPY.display((float) 0.5, (float)0.55, (float)0.1, (float)0.1, 25, player.getLocation().add(0, 1, 0), player);
				}//Close if player is currently mystified
				else{
					Messages.send(player, ChatColor.DARK_PURPLE, config.getString("Messages.Malaria.CureNotMalaria"));
				}//Close if player does not have malaria
			}//Close if held item is a "Bug Spray"
		}//Close if action is a right click
	}//Close onPlayerClick event
	
	private static void bandageHandler(Player player, int healthIncrease, boolean isBleeding){
		if (isBleeding){
			PlayerStatus.removeCondition(player, Condition.BLEEDING);
			Messages.send(player, ChatColor.DARK_RED, config.getString("Messages.Bleeding.UseBandage"));
		}else{
			Messages.send(player, ChatColor.DARK_RED, config.getString("Messages.Bleeding.UseBandageHeal"));
		}
		
		player.setHealth(player.getHealth() + healthIncrease);
		removeCurrentItem(player);
		ParticleEffect.VILLAGER_HAPPY.display((float) 0.5, (float)0.55, (float)0.1, (float)0.1, 25, player.getLocation().add(0, 1, 0), player);
	}//Close bandageHandler method
	
	private static void removeCurrentItem(Player player){
		if (player.getItemInHand().getAmount() > 1){
			player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
		}//Close if there's more than one bandage
		else{
			player.getInventory().setItemInHand(new ItemStack(Material.AIR));
		}//Close if there's one bandage
	}//Close removeCurrentItem method
	
	private boolean itemEquals(Player player, Material itemType, String itemName){
		if (player.getItemInHand().getType() == itemType && player.getItemInHand().hasItemMeta()
				&& player.getItemInHand().getItemMeta().getDisplayName().equals(itemName)){
			return true;
		}else{
			return false;
		}//Close if the item isn't equal to criteria given
	}//Close itemEquals method
}//Close class