package me.choco.conditions.events;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import me.choco.conditions.api.Condition;
import me.choco.conditions.api.PlayerStatus;
import me.choco.conditions.particles.ParticleEffect;
import me.choco.conditions.particles.ParticleEffect.BlockData;
import me.choco.conditions.particles.ParticleEffect.ParticleData;

public class PlayerDeath implements Listener{
	public void spawnZombie(Location location, Player player){
		Zombie zombie = (Zombie) location.getWorld().spawn(player.getLocation(), Zombie.class);
		ItemStack[] playerArmour = player.getInventory().getArmorContents();
		zombie.setCustomName(ChatColor.DARK_GREEN + player.getDisplayName());
		zombie.setCustomNameVisible(true);
		zombie.getEquipment().setArmorContents(playerArmour);
		zombie.getEquipment().setItemInHand(player.getInventory().getItemInHand());
	}//Close spawnZombie method
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event){
		Player killed = event.getEntity();
		String temp = event.getDeathMessage();
		String deathMessage = ChatColor.stripColor(temp);
		if (PlayerStatus.isBleeding(killed)){
			Random random = new Random();
			int randomInt = random.nextInt(2);
			if (deathMessage.equals(killed.getName() + " died")){
				if (randomInt == 0){
					event.setDeathMessage(killed.getDisplayName() + " has bled to death");
				}//Close death message 1
				if (randomInt == 1){
					event.setDeathMessage(killed.getDisplayName() + " bled out");
				}//Close death message 2
				else{
					event.setDeathMessage(killed.getDisplayName() + " died to chronic bleeding");
				}//Close death message 3
			}//Close if player died abruptly
		}//Close if player died to bleeding
		if (PlayerStatus.isInfected(killed)){
			Random random = new Random();
			int randomInt = random.nextInt(2);
			if (deathMessage.equals(killed.getName() + " died")){
				if (randomInt == 0){
					event.setDeathMessage(killed.getDisplayName() + " has fallen to zombie infection");
				}//Close death message 1
				if (randomInt == 1){
					event.setDeathMessage(killed.getDisplayName() + " forgot not to hug a zombie");
				}//Close death message 2
				if (randomInt == 2){
					event.setDeathMessage(killed.getDisplayName() + " forgot what the cure does");
				}//Close death message 3
			}//Close if death message = "player died"
			spawnZombie(killed.getLocation(), killed);
			killed.getWorld().playSound(killed.getLocation(), Sound.ZOMBIE_DEATH, 3, 0);
		}//Close if player died to infection
		if (PlayerStatus.isFrostbitten(killed)){
			Random random = new Random();
			int randomInt = random.nextInt(2);
			if (deathMessage.equals(killed.getName() + " died")){
				if (randomInt == 0){
					event.setDeathMessage(killed.getDisplayName() + " has frozen over");
				}//Close death message 1
				if (randomInt == 1){
					event.setDeathMessage(killed.getDisplayName() + " died due to frostbite");
				}//Close death message 2
				if (randomInt == 2){
					event.setDeathMessage(killed.getDisplayName() + " stayed in a cold biome too long");
				}//Close death message 3
			}//Close if death message = "player died"
		}//Close if player died to frostbitten
		if (PlayerStatus.isFoodPoisoned(killed)){
			Random random = new Random();
			int randomInt = random.nextInt(2);
			if (deathMessage.equals(killed.getName() + " died")){
				if (randomInt == 0){
					event.setDeathMessage(killed.getDisplayName() + " has died of food poisoning");
				}//Close death message 1
				if (randomInt == 1){
					event.setDeathMessage(killed.getDisplayName() + " may have ate too much raw beef");
				}//Close death message 2
				if (randomInt == 2){
					event.setDeathMessage(killed.getDisplayName() + " lost their apetite due to food poisoning");
				}//Close death message 3
			}//Close if death message = "player died"
		}//Close if player died to food poisoning
		if (PlayerStatus.hasMalaria(killed)){
			Random random = new Random();
			int randomInt = random.nextInt(2);
			if (deathMessage.equals(killed.getName() + " died")){
				if (randomInt == 0){
					event.setDeathMessage(killed.getDisplayName() + " has died of malaria");
				}//Close death message 1
				if (randomInt == 1){
					event.setDeathMessage(killed.getDisplayName() + " was around too many mosquitos");
				}//Close death message 2
				if (randomInt == 2){
					event.setDeathMessage(killed.getDisplayName() + " tried to befriend the mosquitos");
				}//Close death message 3
			}//Close if death message = "player died"
		}//Close if player died to malaria
		if (killed.getKiller() != null){
			ParticleData ParticleData = new BlockData(Material.REDSTONE_BLOCK, (byte) 0);
			ParticleEffect.BLOCK_DUST.display(ParticleData, (float) 0.25, (float) 0.55, (float) 0.25, (float) 0.15, 50, killed.getLocation().add(0, 1, 0), killed, killed.getKiller());
		}//Close if player was killed by something
		else{
			ParticleData ParticleData = new BlockData(Material.REDSTONE_BLOCK, (byte) 0);
			ParticleEffect.BLOCK_DUST.display(ParticleData, (float) 0.25, (float) 1, (float) 0.25, (float) 0.15, 50, killed.getLocation().add(0, 1, 0), killed);
		}//Close if player died by themselves?
		
		if (PlayerStatus.isBleeding(killed))
			PlayerStatus.removeCondition(killed, Condition.BLEEDING);
		if (PlayerStatus.isInfected(killed))
			PlayerStatus.removeCondition(killed, Condition.INFECTION);
		if (PlayerStatus.isVaccinated(killed))
			PlayerStatus.removeCondition(killed, Condition.VACCINATED);
		if (PlayerStatus.isFoodPoisoned(killed))
			PlayerStatus.removeCondition(killed, Condition.FOOD_POISONING);
		if (PlayerStatus.isFrostbitten(killed))
			PlayerStatus.removeCondition(killed, Condition.FROSTBITE);
		if (PlayerStatus.isMystified(killed))
			PlayerStatus.removeCondition(killed, Condition.MYSTOPHOBIA);
		if (PlayerStatus.isHungry(killed))
			PlayerStatus.removeCondition(killed, Condition.HUNGRY);
		if (PlayerStatus.hasMalaria(killed))
			PlayerStatus.removeCondition(killed, Condition.MALARIA);
	}//Close onPlayerDeath event
}//Close class

//147 lines