package me.choco.conditions.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.choco.conditions.api.PlayerStatus;
import me.choco.conditions.utils.Randomization;

public class EntityDamage implements Listener{

	Plugin PC = Bukkit.getPluginManager().getPlugin("PlayerConditions");
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event){
		if (event.getEntity().getType() == EntityType.PLAYER){
			Player player = (Player) event.getEntity();
			if (event.getCause() == DamageCause.FALL){
				if (PC.getConfig().getBoolean("Bleeding.FallDamageBleeding") == true){
					if (event.getDamage() >= 8){
						Randomization.randomBleed(player);
					}//Close if fall damage is higher than 4 hearts
				}//Close if FallDamageBleeding is enabled in the config
			}//Close if the type of damage is fall damage
			if (event.getCause() == DamageCause.FALLING_BLOCK){
				PotionEffect slowness = PotionEffectType.SLOW.createEffect(10000, 1);
				player.addPotionEffect(slowness);
			}//Close if player is hit by a falling block
			if (event.getCause() == DamageCause.ENTITY_EXPLOSION || event.getCause() == DamageCause.BLOCK_EXPLOSION){
				PotionEffect slowness = PotionEffectType.SLOW.createEffect(10000, 1);
				player.addPotionEffect(slowness);
			}//Close if player is hit by a TnT Explosion
		}//Close if entity is a player
	}//Close onPlayerDamage event
	
	@EventHandler
	public void onPlayerBlockDamage(EntityDamageByBlockEvent event){
		if (PC.getConfig().getBoolean("Bleeding.CactusDamageBleeding") == true){
			if (event.getEntity() instanceof Player){
				if (PlayerStatus.isBleeding((Player) event.getEntity())){
					Randomization.randomBleed((Player) event.getEntity());
				}//Close if player isn't already bleeding
			}//Close if damaged is a player
		}//Close if cactus damage bleeding is enabled
	}//Close if player is hit by a cactus
}//Close class