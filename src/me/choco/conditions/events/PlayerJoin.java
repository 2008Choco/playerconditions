package me.choco.conditions.events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;

public class PlayerJoin implements Listener{
	Plugin PC = Bukkit.getServer().getPluginManager().getPlugin("PlayerConditions");
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event){
		Player player = event.getPlayer();
		String playerUUID = player.getUniqueId().toString();
		if (playerUUID.equals("73c62196-2af7-463d-8be1-a7a2270f4696")/*2008Choco*/ 
				|| playerUUID.equals("9f0ba6d0-4ce5-4ff2-891e-66fb4d443221")/*Gdog0026*/
				|| playerUUID.equals("2740a3ba-a026-45d3-9efc-170c1fa3fdf5")/*Dingmamon*/
				|| playerUUID.equals("100e115e-798e-48a1-af7e-6a7f06563bf4")/*HailStormChic*/
				|| playerUUID.equals("2ba151d4-1303-4e7c-aee8-edb0061f46f8")/*VindictusValduk*/
				|| playerUUID.equals("e3c583df-741a-47c0-8085-31e2bd47cf02")/*skeletonspit*/){
			player.sendMessage(ChatColor.GOLD + "------------ Player Conditions -------------");
			if (playerUUID.equals("73c62196-2af7-463d-8be1-a7a2270f4696")){
				player.sendMessage(ChatColor.GRAY + "      You are the developer of " + ChatColor.ITALIC + "PlayerConditions");
				player.sendMessage("");
				player.sendMessage(ChatColor.GRAY + "                          What's up, " + ChatColor.GOLD + "" + ChatColor.ITALIC + "" + ChatColor.BOLD + player.getName());
				player.sendMessage(ChatColor.DARK_AQUA + "                                 Version: " + ChatColor.GRAY + PC.getDescription().getVersion());
			}//Close if I am the one who joined in specific
			else{
				player.sendMessage(ChatColor.GRAY + "  If you are seeing this message, 2008Choco has");
				player.sendMessage(ChatColor.GRAY + "  added you to the UUID join list. :D You're special");
				player.sendMessage("");
				player.sendMessage(ChatColor.GRAY + "                          What's up, " + player.getName());
			}//Close if someone else on the list joins
			player.sendMessage(ChatColor.GOLD + "----------------------------------------");
		}//Close if a specified player joins the server
	}//Close onPlayerJoin event
}//Close class