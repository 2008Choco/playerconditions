package me.choco.conditions.events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.choco.conditions.api.Condition;
import me.choco.conditions.api.PlayerStatus;
import me.choco.conditions.particles.ParticleEffect;
import me.choco.conditions.utils.Randomization;

public class PlayerItemConsume implements Listener{
	@EventHandler
	public void onPlayerItemConsume(final PlayerItemConsumeEvent event){
		final Plugin PC = Bukkit.getPluginManager().getPlugin("PlayerConditions");
		ItemStack item = event.getItem();
		final Player player = event.getPlayer();
		if (PC.getConfig().getBoolean("FoodPoisoningEnabled") == true){
			if (item.getType() == Material.RAW_BEEF || item.getType() == Material.RAW_CHICKEN || item.getType() == Material.RAW_FISH 
					|| item.getType() == Material.PORK || item.getType()== Material.RABBIT || item.getType() == Material.MUTTON
					|| item.getType() == Material.ROTTEN_FLESH){
				if (!PlayerStatus.isVaccinated(player)){
					Randomization.randomPoison(player);
				}//Close if player is not vaccinated
			}//Close if raw steak, raw chicken, raw fish, or raw pork chops are eaten
		}//Close if foodPoisoning is enabled
		if (itemEquals(player, Material.MUSHROOM_SOUP, ChatColor.GREEN + "Antibiotics")){
			PlayerStatus.setCondition(player, Condition.VACCINATED);
			if (!PlayerStatus.isFoodPoisoned(player) && !PlayerStatus.isVaccinated(player)){
				PlayerStatus.setCondition(player, Condition.VACCINATED);
				player.sendMessage(ChatColor.BLUE + "Condition> " + ChatColor.GRAY + PC.getConfig().getString("Messages.Antibiotics.Protected"));
				ParticleEffect.VILLAGER_HAPPY.display((float) 0.1, (float)0.55, (float)0.1, (float)0.1, 25, player.getLocation().add(0, 1, 0), player);
			}//Close if player isn't food poisoned and not vaccinated
			else if (PlayerStatus.isFoodPoisoned(player) && !PlayerStatus.isVaccinated(player)){
				PlayerStatus.setCondition(player, Condition.VACCINATED);
				PlayerStatus.removeCondition(player, Condition.FOOD_POISONING);
				player.sendMessage(ChatColor.BLUE + "Condition> " + ChatColor.GRAY + PC.getConfig().getString("Messages.Antibiotics.CuredFoodPoisoning"));
				player.removePotionEffect(PotionEffectType.SLOW);
				player.removePotionEffect(PotionEffectType.CONFUSION);
				player.removePotionEffect(PotionEffectType.HUNGER);
				ParticleEffect.VILLAGER_HAPPY.display((float) 0.5, (float)0.55, (float)0.1, (float)0.1, 25, player.getLocation().add(0, 1, 0), player);
			}//Close if player is food poisoned but not vaccinated
			else if (PlayerStatus.isVaccinated(player)){
				event.setCancelled(true);
				player.sendMessage(ChatColor.BLUE + "Condition> " + ChatColor.GRAY + PC.getConfig().getString("Messages.Antibiotics.AlreadyVaccinated"));
			}//Close if player isn't food poisoned, and vaccinated
			PC.getServer().getScheduler().scheduleSyncDelayedTask(PC, new Runnable(){
				@Override
				public void run(){
					PlayerStatus.removeCondition(player, Condition.VACCINATED);
					player.sendMessage(ChatColor.LIGHT_PURPLE + "Condition> " + ChatColor.GRAY + PC.getConfig().getString("Messages.Antibiotics.ProtectionWornOff"));
				}//Close runnable
			}, 72000L);
		}//Close if item == anti-biotic
		if (itemEquals(player, Material.MUSHROOM_SOUP, ChatColor.GOLD + "Chicken Noodle Soup")){
			if (PlayerStatus.isFrostbitten(player)){
				PlayerStatus.removeCondition(player, Condition.FROSTBITE);
				player.sendMessage(ChatColor.AQUA + "Condition> " + ChatColor.GRAY + "The chicken noodle soup warmed you up. Mmm");
				ParticleEffect.VILLAGER_HAPPY.display((float) 0.5, (float)0.55, (float)0.1, (float)0.1, 25, player.getLocation().add(0, 1, 0), player);
			}//Close if player is frostbitten
			else{
				event.setCancelled(true);
				player.sendMessage(ChatColor.AQUA + "Condition> " + ChatColor.GRAY + "You are not frostbitten. You don't need to waste this");
			}//Close if player is NOT frostbitten
		}//Close if item == chickenNoodleSoup
		if (item.getType() == Material.APPLE){
			PotionEffect regeneration = PotionEffectType.REGENERATION.createEffect(60, 1);
			player.addPotionEffect(regeneration);
		}//Close if a healthy item is eaten :)
	}//Close onPlayerItemConsume event
	
	private boolean itemEquals(Player player, Material itemType, String itemName){
		if (player.getItemInHand().getType() == itemType && player.getItemInHand().hasItemMeta()
				&& player.getItemInHand().getItemMeta().getDisplayName().equals(itemName)){
			return true;
		}else{
			return false;
		}//Close if the item isn't equal to criteria given
	}//Close itemEquals method
}//Close class