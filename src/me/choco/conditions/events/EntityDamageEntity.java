package me.choco.conditions.events;

import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Slime;
import org.bukkit.entity.Spider;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import me.choco.conditions.api.PlayerStatus;
import me.choco.conditions.particles.ParticleEffect;
import me.choco.conditions.particles.ParticleEffect.BlockData;
import me.choco.conditions.particles.ParticleEffect.ParticleData;
import me.choco.conditions.utils.Randomization;

public class EntityDamageEntity implements Listener{
	@EventHandler
	public void onEntityDamageEntity(EntityDamageByEntityEvent event){
		if (event.getEntity() instanceof Player){
			
			Player damaged = (Player) event.getEntity();
			Entity damager = event.getDamager();
			ParticleData ParticleData = new BlockData(Material.REDSTONE_BLOCK, (byte) 0);
			
			if (damager instanceof Player){
				ParticleEffect.BLOCK_DUST.display(ParticleData, (float) 0.15, (float) 0.55, (float) 0.15, (float) 0.1, 20, damaged.getLocation().add(0, 1, 0), (Player) damager);
				if (!PlayerStatus.isBleeding(damaged)){
					Randomization.randomBleed(damaged);
				}//Close if player isn't already bleeding
			}//Close if damager is player
			
			if (damager instanceof Zombie){
				if (!PlayerStatus.isInfected(damaged)){
					Randomization.randomInfect(damaged);
				}//Close if player isn't already infected	
			}//Close if damager is zombie
			
			if (damager instanceof Enderman){
				if (!PlayerStatus.isMystified(damaged)){
					Randomization.randomEnder(damaged);
				}//Close if player isn't already infected
				else{
					event.setDamage(event.getDamage() + 2);
				}//Close if player is mystified
			}//Close if damager is enderman
			
			if (damager instanceof Slime){
				ParticleEffect.SLIME.display((float) 0.15, (float) 0.55, (float) 0.15, (float) 0.1, 20, damaged.getLocation().add(0, 1, 0), damaged);
				Randomization.randomSlimed(damaged); 
			}//Close if damager is slime
			
			if (damager instanceof Arrow){
				Arrow arrow = (Arrow) damager;
				if (arrow.getShooter() instanceof Skeleton){
					//TODO: Osteophobia
				}//Close if a skeleton shot the arrow
			}//Close if damager is arrow (skeleton)
			
			if (damager instanceof Spider){
				if (!PlayerStatus.isArachnophobic(damaged)){
					Randomization.randomizeArachnophobia(damaged);
				}//Close if player is not already arachnophobic
				else{
					event.setDamage(event.getDamage() + 2);;
				}//Close if player is arachnophobic
			}//Close if damager is spider
			
			ParticleEffect.BLOCK_DUST.display(ParticleData, (float) 0.15, (float) 0.55, (float) 0.15, (float) 0.1, 20, damaged.getLocation().add(0, 1, 0), damaged);
		}//Close if Player hits Player
	}//Close onEntityDamageEntity method
}//Close class