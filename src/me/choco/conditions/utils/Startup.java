package me.choco.conditions.utils;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import me.choco.conditions.events.EntityDamageEntity;
import me.choco.conditions.events.PlayerClickEvent;
import me.choco.conditions.events.PlayerDeath;
import me.choco.conditions.events.PlayerItemConsume;
import me.choco.conditions.events.PlayerJoin;
import me.choco.conditions.utils.commands.ConditionsCommand;

@SuppressWarnings("deprecation")
public class Startup {
	public static void registerCommands(){
		Bukkit.getPluginCommand("condition").setExecutor(new ConditionsCommand());
	}//Close registerCommands method
	
	public static void registerEvents(){
		Plugin PC = Bukkit.getPluginManager().getPlugin("PlayerConditions");
		Bukkit.getServer().getPluginManager().registerEvents(new EntityDamageEntity(), PC);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerDeath(), PC);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerItemConsume(), PC);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerClickEvent(), PC);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoin(), PC);
	}//Close registerEvents method
	
	public static void loadItems(){
		ItemStack bandage = createItem(Material.PAPER, ChatColor.RED + "Bandage", new ArrayList<String>(Arrays.asList(
				ChatColor.GRAY + "Right click with this to",
				ChatColor.GRAY + "stop yourself from bleeding")));
		
		ItemStack cure = createItem(Material.GHAST_TEAR, ChatColor.GREEN + "Infection Cure", new ArrayList<String>(Arrays.asList(
				ChatColor.GRAY + "Right click with this to",
				ChatColor.GRAY + "cure yourself from an infection")));
		
		ItemStack antibiotic = createItem(Material.MUSHROOM_SOUP, ChatColor.GREEN + "Antibiotics", new ArrayList<String>(Arrays.asList(
				ChatColor.GRAY + "Eat this item to prevent",
				ChatColor.GRAY + "yourself from diseases")));
		
		ItemStack chickenNoodleSoup = createItem(Material.MUSHROOM_SOUP, ChatColor.GOLD + "Chicken Noodle Soup", new ArrayList<String>(Arrays.asList(
				ChatColor.GRAY + "Eat this item to warm",
				ChatColor.GRAY + "yourself from frostbite")));
		
		ItemStack flowerOfPurity = createItem(Material.RED_ROSE, ChatColor.LIGHT_PURPLE + "Flower of Purity", new ArrayList<String>(Arrays.asList(
				ChatColor.GRAY + "Something about this flower",
				ChatColor.GRAY + "seems very calming to you")));
		
		ItemStack web = createItem(Material.WEB, ChatColor.DARK_GRAY + "I promise. It helps", new ArrayList<String>(Arrays.asList(
				ChatColor.GRAY + "For whatever reason, this",
				ChatColor.GRAY + "cobweb cures arachnophobia?",
				ChatColor.GRAY + "(Choco was lazy. Let's be honest)")));
		
		ItemStack malariaCure = createItem(Material.EYE_OF_ENDER, ChatColor.DARK_PURPLE + "Bug Spray", new ArrayList<String>(Arrays.asList(
				ChatColor.GRAY + "This mysterious ender force",
				ChatColor.GRAY + "will cure your malaria symptoms")));
		
		ShapedRecipe bandageRecipe1 = new ShapedRecipe(bandage).shape("   ","   ","WRW").setIngredient('W', Material.WOOL).setIngredient('R', Material.INK_SACK, 1);
		ShapedRecipe bandageRecipe2 = new ShapedRecipe(bandage).shape("   ","WRW","   ").setIngredient('W', Material.WOOL).setIngredient('R', Material.INK_SACK, 1);
		ShapedRecipe bandageRecipe3 = new ShapedRecipe(bandage).shape("WRW","   ","   ").setIngredient('W', Material.WOOL).setIngredient('R', Material.INK_SACK, 1);
		ShapedRecipe cureRecipe = new ShapedRecipe(cure).shape(" G ","GMG"," G ").setIngredient('G', Material.INK_SACK, 10).setIngredient('M', Material.MILK_BUCKET);
		ShapelessRecipe antibioticRecipe = new ShapelessRecipe(antibiotic).addIngredient(2, Material.SUGAR_CANE).addIngredient(2, Material.SEEDS).addIngredient(Material.BOWL);
		ShapelessRecipe chickenNoodleSoupRecipe = new ShapelessRecipe(chickenNoodleSoup).addIngredient(2, Material.SUGAR_CANE).addIngredient(2, Material.COOKED_CHICKEN).addIngredient(Material.BOWL);
		ShapedRecipe flowerOfPurityRecipe = new ShapedRecipe(flowerOfPurity).shape(" R "," F ","SSS").setIngredient('R', Material.INK_SACK, 1).setIngredient('F', Material.RED_ROSE).setIngredient('S', Material.SEEDS);
		ShapedRecipe webRecipe = new ShapedRecipe(web).shape("SSS","SSS","SSS").setIngredient('S', Material.STRING);
		ShapedRecipe malariaCureRecipe = new ShapedRecipe(malariaCure).shape("V V"," S ","V V").setIngredient('V', Material.VINE).setIngredient('S', Material.SLIME_BALL);
		
		Bukkit.getServer().addRecipe(bandageRecipe1);
		Bukkit.getServer().addRecipe(bandageRecipe2);
		Bukkit.getServer().addRecipe(bandageRecipe3);
		Bukkit.getServer().addRecipe(cureRecipe);
		Bukkit.getServer().addRecipe(antibioticRecipe);
		Bukkit.getServer().addRecipe(chickenNoodleSoupRecipe);
		Bukkit.getServer().addRecipe(flowerOfPurityRecipe);
		Bukkit.getServer().addRecipe(webRecipe);
		Bukkit.getServer().addRecipe(malariaCureRecipe);
	}//Close loadItems method
	
	private static ItemStack createItem(Material itemMaterial, String itemName, ArrayList<String> itemLore){
		ItemStack item = new ItemStack(itemMaterial);
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName(itemName);
		itemMeta.setLore(itemLore);
		item.setItemMeta(itemMeta);
		return item;
	}//Close createItem method
}//Close class