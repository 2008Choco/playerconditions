package me.choco.conditions.utils;

import java.util.Random;

import me.choco.conditions.Conditions;
import me.choco.conditions.api.Condition;
import me.choco.conditions.api.PlayerStatus;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Randomization implements Listener{
	
	final static Plugin PC = Bukkit.getPluginManager().getPlugin("PlayerConditions");
	static Random random = new Random();
	
	public static void randomizeArachnophobia(Player damaged){
		int arachnophobiaChance = PC.getConfig().getInt("Chances.ArachnophobiaChance");
		
		//                                                          5 out of 100 (5%) chance to start bleeding (DEFAULT)
		
		int randomInt = random.nextInt(99);
		randomInt++;
		
		if (randomInt <= arachnophobiaChance){
			PlayerStatus.setCondition(damaged, Condition.ARACHNOPHOBIA);
			damaged.sendMessage(ChatColor.DARK_GRAY + "Condition> " + ChatColor.GRAY + PC.getConfig().getString("Messages.Arachnophobia.GetArachnophobia"));
		}//Close if randomInt == 1
	}//Close randomizeArachnophobia method
	
	
	public static void randomBleed(Player damaged){
		int bleedingChance = PC.getConfig().getInt("Chances.BleedingChance");
		
		//                                                          5 out of 100 (5%) chance to start bleeding (DEFAULT)
		
		int randomInt = random.nextInt(99);
		randomInt++;
		
		if (randomInt <= bleedingChance){
			PlayerStatus.setCondition(damaged, Condition.BLEEDING);
			damaged.sendMessage(ChatColor.DARK_RED + "Condition> " + ChatColor.GRAY + PC.getConfig().getString("Messages.Bleeding.StartBleeding"));
		}//Close if randomInt == 1
	}//Close randomBleed method
	
	
	public static void randomConfusion40(Player player){
		int confusionChance = PC.getConfig().getInt("Chances.ConfusionChance");
		
		//                                                          40 out of 100 (40%) chance to receive nausea (DEFAULT)
		
		int randomInt = random.nextInt(99);
		randomInt++;
		
		if (randomInt <= confusionChance){
			PotionEffect confusion = PotionEffectType.CONFUSION.createEffect(600, 1);
			player.addPotionEffect(confusion);
		}//Close if randomInt == 1
	}//Close randomBlindness method
	
	
	public static void randomEnder(Player damaged){
		int mystophobiaChance = PC.getConfig().getInt("Chances.MystophobiaChance");
		
		//                                                          10 out of 100 (10%) chance to become mystified (DEFAULT)
		
		int randomInt = random.nextInt(99);
		randomInt++;
		
		if (randomInt <= mystophobiaChance){
			PlayerStatus.setCondition(damaged, Condition.MYSTOPHOBIA);
			damaged.sendMessage(ChatColor.DARK_GRAY + "Condition> " + ChatColor.GRAY + PC.getConfig().getString("Messages.Mystophobia.GetMystophobia"));
		}//Close if randomInt == 1
	}//Close randomEnder method
	
	
	public static void randomPoison(final Player damaged){
		int foodPoisoningChance = PC.getConfig().getInt("Chances.FoodPoisoningChance");
		
		//                                                          25 out of 100 (25%) chance to become poisoned (DEFAULT)
		
		int randomInt = random.nextInt(100);
		randomInt++;
		
		if (randomInt <= foodPoisoningChance){
			if (!PlayerStatus.isFoodPoisoned(damaged)){
				if (!PlayerStatus.isVaccinated(damaged)){
					PotionEffect confusion = PotionEffectType.CONFUSION.createEffect(2400, 1);
					PotionEffect hunger = PotionEffectType.HUNGER.createEffect(1200, 0);
					PotionEffect slowness = PotionEffectType.SLOW.createEffect(1200, 1);
					damaged.addPotionEffect(confusion);
					damaged.addPotionEffect(hunger);
					damaged.addPotionEffect(slowness);
					damaged.sendMessage(ChatColor.LIGHT_PURPLE + "Condition> " + ChatColor.GRAY + PC.getConfig().getString("Messages.FoodPoisoning.GetFoodPoisoning"));
					damaged.getWorld().playSound(damaged.getLocation(), Sound.BURP, 3, 0);
					PlayerStatus.setCondition(damaged, Condition.FOOD_POISONING);
					PC.getServer().getScheduler().scheduleSyncDelayedTask(PC, new Runnable(){
						@Override
						public void run() {
							PlayerStatus.removeCondition(damaged, Condition.FOOD_POISONING);
						}//Close runnable
					}, 600L);
				}//Close if player is not vaccinated
				else{
					damaged.sendMessage(ChatColor.LIGHT_PURPLE + "Condition> " + ChatColor.GRAY + PC.getConfig().getString("Messages.Antibiotics.PreventedFoodPoisoning"));
				}//Close if player is vaccinated
			}//Close if player is not already food poisoned
		}//Close if randomInt == 1
	}//Close foodPoisoningRandomizer
	
	
	public static void randomFrostbite(final Player damaged){
		int frostbiteChance = Conditions.frostbiteChanceInt;
		
		//                                                    DEFAULT: 20 out of 2000 (1%) chance to become frostbitten (DEFAULT)
		
		int randomInt = random.nextInt(2000);
		randomInt++;
		
		if (randomInt <= 1 && randomInt < frostbiteChance){
			if (!PlayerStatus.isFrostbitten(damaged)){
				PlayerStatus.setCondition(damaged, Condition.FROSTBITE);
				damaged.sendMessage(ChatColor.AQUA + "Condition> " + ChatColor.GRAY + PC.getConfig().getString("Messages.Frostbite.GetFrostbite"));
			}//Close if player isn't already frostbitten
		}//Close if randomInt == 1
	}//Close frostbiteRandomizer
	
	
	public static void randomInfect(Player damaged){
		int infectionChance = PC.getConfig().getInt("Chances.InfectionChance");
		
		//                                                          5 out of 100 (5%) chance to become infected (DEFAULT)
		
		int randomInt = random.nextInt(99);
		randomInt++;
		
		if (randomInt <= infectionChance){
			PlayerStatus.setCondition(damaged, Condition.INFECTION);
			damaged.sendMessage(ChatColor.DARK_GREEN + "Condition> " + ChatColor.GRAY + PC.getConfig().getString("Messages.ZombieInfection.GetInfected"));
		}//Close if randomInt == 1
	}//Close randomInfect method
	
	
	public static void randomMalaria(Player damaged){
		int malariaChance = PC.getConfig().getInt("Chances.MalariaChance");
		
		//                                                    DEFAULT: 1 out of 100 (1%) chance to become frostbitten (DEFAULT)
		
		int randomInt = random.nextInt(99);
		randomInt++;
		
		if (randomInt <= malariaChance){
			if (!PlayerStatus.hasMalaria(damaged)){
				PlayerStatus.setCondition(damaged, Condition.MALARIA);
				damaged.sendMessage(ChatColor.DARK_PURPLE + "Condition> " + ChatColor.GRAY + PC.getConfig().getString("Messages.Malaria.GetMalaria"));
			}//Close if player isn't already frostbitten
		}//Close if randomInt == 1
	}//Close randomMalaria method
	
	
	public static void randomMystophobiaEffects(Player damaged){
		
		//                                                          1 out of 5 (20%) chance to become infected (DEFAULT)
		
		int randomInt = random.nextInt(4);
		randomInt++;
		int randomInt2 = random.nextInt(4);
		randomInt2++;
		int randomInt3 = random.nextInt(4);
		randomInt3++;
		
		if (randomInt == 1){
			PotionEffect slowness = PotionEffectType.SLOW.createEffect(350, 1);
			damaged.addPotionEffect(slowness);
		}//Close if randomInt == 1
		
		if (randomInt2 == 1){
			PotionEffect blindness = PotionEffectType.BLINDNESS.createEffect(700, 0);
			damaged.addPotionEffect(blindness);
		}//Close if randomInt2 == 1
		
		if (randomInt3 == 1){
			PotionEffect confusion = PotionEffectType.CONFUSION.createEffect(700, 0);
			damaged.addPotionEffect(confusion);
		}//Close if randomInt3 == 1
	}//Close randomMystophobiaEffects
	
	
	public static void randomSlimed(Player damaged){
		int slimedChance = PC.getConfig().getInt("Chances.SlimedChance");
		
		//                                                          15 out of 100 (15%) chance to become infected (DEFAULT)
		
		int randomInt = random.nextInt(99);
		randomInt++;
		
		if (randomInt <= slimedChance){
			PlayerStatus.setCondition(damaged, Condition.SLIMED);
			damaged.sendMessage(ChatColor.GREEN + "Condition> " + ChatColor.GRAY + PC.getConfig().getString("Messages.Slimed.GetSlimed"));
			PotionEffect slowness = PotionEffectType.SLOW.createEffect(1500, 1);
			damaged.addPotionEffect(slowness);
		}//Close randomInt chance
	}//Close randomSlimed method
}//Close class