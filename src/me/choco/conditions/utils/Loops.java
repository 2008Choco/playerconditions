package me.choco.conditions.utils;

import me.choco.conditions.Conditions;
import me.choco.conditions.api.Condition;
import me.choco.conditions.api.PlayerStatus;
import me.choco.conditions.particles.ParticleEffect;
import me.choco.conditions.particles.ParticleEffect.BlockData;
import me.choco.conditions.particles.ParticleEffect.ParticleData;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Loops implements Listener{
	final static Plugin PC = Bukkit.getPluginManager().getPlugin("PlayerConditions");
	public static void bleedingLoop(){
		PC.getServer().getScheduler().scheduleSyncRepeatingTask(PC, new Runnable(){
			@Override
			public void run() {
				for (Player player : Bukkit.getOnlinePlayers()){
					if (PlayerStatus.isBleeding(player)){
						player.damage(PC.getConfig().getDouble("Bleeding.BleedDamage"));
						ParticleData ParticleData = new BlockData(Material.REDSTONE_BLOCK, (byte) 0);
						ParticleEffect.BLOCK_DUST.display(ParticleData, (float) 0.15, (float) 0.55, (float) 0.15, (float) 0.1, 20, player.getLocation().add(0, 1, 0), player);
					}//Close if player is bleeding
				}//Close list through every player
			}//Close run
		}, 0L, PC.getConfig().getLong("Bleeding.BleedDamageDelay"));
	}//Close bandageLoop method
	
	public static void infectionLoop(){
		PC.getServer().getScheduler().scheduleSyncRepeatingTask(PC, new Runnable(){
			@Override
			public void run(){
				for (Player player : Bukkit.getOnlinePlayers()){
					if (PlayerStatus.isInfected(player)){
						if (PC.getConfig().getBoolean("ZombieInfection.InfectionsInflictDamage") == true){
							player.damage(PC.getConfig().getDouble("ZombieInfection.InfectionDamage"));
						}//Close if dealing damage is set to true
						if (PC.getConfig().getBoolean("ZombieInfection.InfectionPeriodicalConfusion") == true){
							Randomization.randomConfusion40(player);
						}//Close if periodical blindness is set to true
						player.playSound(player.getLocation(), Sound.ZOMBIE_IDLE, 1, 0);
					}//Close if player is infected
				}//Close list through every player
			}//Close run
		}, 0L, PC.getConfig().getLong("ZombieInfection.InfectionDamageDelay"));
	}//Close infectionLoop method
	
	public static void vertigoLoop(){
		PC.getServer().getScheduler().scheduleSyncRepeatingTask(PC, new Runnable(){
			@Override
			public void run(){
				for (Player player : Bukkit.getOnlinePlayers()){
					if (player.getLocation().getBlockY() >= PC.getConfig().getInt("Vertigo.VertigoHeight")){
						Randomization.randomConfusion40(player);
					}//Close if player is above x blocks
				}//Close list through every player
			}//Close run
		}, 0L, 40L);
	}//Close heightLoop method
	
	public static void thalassophobiaLoop(){
		PC.getServer().getScheduler().scheduleSyncRepeatingTask(PC, new Runnable(){
			@Override
			public void run(){
				for (Player player : Bukkit.getOnlinePlayers()){
					if (player.getRemainingAir() == 7 || player.getRemainingAir() == 6){
						PotionEffect slowness = PotionEffectType.SLOW.createEffect(500, 1);
						player.addPotionEffect(slowness);
					}//Close if players air is set to 19
					if (player.getRemainingAir() == 5 || player.getRemainingAir() == 4 || player.getRemainingAir() == 3){
						PotionEffect slowness = PotionEffectType.SLOW.createEffect(500, 2);
						player.addPotionEffect(slowness);
					}//Close if players air is set to 19
					if (player.getRemainingAir() == 2 || player.getRemainingAir() == 1){
						PotionEffect slowness = PotionEffectType.SLOW.createEffect(500, 3);
						player.addPotionEffect(slowness);
					}//Close if players air is set to 19
					if (player.getRemainingAir() == 0){
						PotionEffect slowness = PotionEffectType.SLOW.createEffect(500, 4);
						player.addPotionEffect(slowness);
					}//Close if players air is set to 19
				}//Close list through every player
			}//Close run
		}, 0L, 20L);
	}//Close thalassophobiaLoop
	
	public static void hungerLoop(){
		PC.getServer().getScheduler().scheduleSyncRepeatingTask(PC, new Runnable(){
			@Override
			public void run(){
				for (Player player : Bukkit.getOnlinePlayers()){
					if (player.getFoodLevel() < 20){
						if (player.getFoodLevel() == 6 || player.getFoodLevel() == 5){
							setHungerSlowness(player, 0);
						}//Close if players hunger is 6
						if (player.getFoodLevel() == 4 || player.getFoodLevel() == 3){
							setHungerSlowness(player, 1);
						}//Close if players hunger is 4
						if (player.getFoodLevel() == 2 || player.getFoodLevel() == 1){
							setHungerSlowness(player, 2);
						}//Close if players hunger is 2
						if (player.getFoodLevel() == 0){
							setHungerSlowness(player, 3);
						}//Close if player has no food levels
						if (player.getFoodLevel() >= 7 && !PlayerStatus.isHungry(player)){
							player.removePotionEffect(PotionEffectType.SLOW);
							PlayerStatus.removeCondition(player, Condition.HUNGRY);
						}//Close if players hunger gets above 6
					}//Close if player has less than 20 food
				}//Close list through every player
			}//Close run
		}, 0L, 20L);
	}//Close hungerLoop
	
	public static void frostbiteLoop(){
		PC.getServer().getScheduler().scheduleSyncRepeatingTask(PC, new Runnable(){
			@Override
			public void run(){
				for (Player player : Bukkit.getOnlinePlayers()){
					Biome biome = player.getLocation().getBlock().getBiome();
					if (biome == Biome.COLD_TAIGA || biome == Biome.COLD_TAIGA_HILLS || biome == Biome.COLD_TAIGA_MOUNTAINS
							|| biome == Biome.ICE_MOUNTAINS || biome == Biome.ICE_PLAINS || biome == Biome.ICE_PLAINS_SPIKES
							&& !PlayerStatus.isFrostbitten(player)){
						if (player.getInventory().getHelmet().getType().equals(Material.LEATHER_HELMET)){
							Conditions.frostbiteChanceInt -= 4;
						}//Close if player is wearing a leather helmet
						if (player.getInventory().getChestplate().getType().equals(Material.LEATHER_CHESTPLATE)){
							Conditions.frostbiteChanceInt -= 7;
						}//Close if player is wearing a leather helmet
						if (player.getInventory().getLeggings().getType().equals(Material.LEATHER_LEGGINGS)){
							Conditions.frostbiteChanceInt -= 6;
						}//Close if player is wearing a leather helmet
						if (player.getInventory().getBoots().getType().equals(Material.LEATHER_BOOTS)){
							Conditions.frostbiteChanceInt -= 3;
						}//Close if player is wearing a leather helmet
						Randomization.randomFrostbite(player);
						Conditions.frostbiteChanceInt = 20;
					}//Close if player is in snow-related biome
					if (PlayerStatus.isFrostbitten(player)){
						player.damage(PC.getConfig().getDouble("Frostbite.FrostbiteDamage"));
					}//Close if player is frostbitten
				}//Close list through every player
			}//Close run
		}, 0L, PC.getConfig().getLong("Frostbite.FrostbiteDamageDelay"));
	}//Close frostbiteLoop
	
	public static void mysticLoop(){
		PC.getServer().getScheduler().scheduleSyncRepeatingTask(PC, new Runnable(){
			@Override
			public void run(){
				for (Player player : Bukkit.getOnlinePlayers()){
					if (PlayerStatus.isMystified(player)){
						Randomization.randomMystophobiaEffects(player);
					}//Close if player is frostbitten
				}//Close list through every player
			}//Close run
		}, 0L, PC.getConfig().getLong("Mystophobia.MystophobiaDelay"));
	}//Close mysticLoop
	
	public static void malariaLoop(){
		PC.getServer().getScheduler().scheduleSyncRepeatingTask(PC, new Runnable(){
			@Override
			public void run(){
				for (Player player : Bukkit.getOnlinePlayers()){
					Biome biome = player.getLocation().getBlock().getBiome();
					if (biome == Biome.SWAMPLAND || biome == Biome.SWAMPLAND_MOUNTAINS && !PlayerStatus.hasMalaria(player)){
						Randomization.randomMalaria(player);
					}//Close if player is in swamp biome
					if (PlayerStatus.hasMalaria(player)){
						player.damage(PC.getConfig().getDouble("Malaria.MalariaDamage"));
						PotionEffect confusion = PotionEffectType.CONFUSION.createEffect(300, 0);
						player.addPotionEffect(confusion);
					}//Close if player has malaria
				}//Close list through every player
			}//Close run
		}, 0L, PC.getConfig().getLong("Malaria.MalariaDamageDelay"));
	}//Close malariaLoop
	
	private static void setHungerSlowness(Player player, int slownessLevel){
		player.removePotionEffect(PotionEffectType.SLOW);
		if (PlayerStatus.isHungry(player)){
			PlayerStatus.setCondition(player, Condition.HUNGRY);
		}//Close if player isn't already in the hunger loop
		PotionEffect slowness = PotionEffectType.SLOW.createEffect(999999, slownessLevel);
		player.addPotionEffect(slowness);
	}
}//Close class