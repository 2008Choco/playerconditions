package me.choco.conditions.utils.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.choco.conditions.api.Condition;
import me.choco.conditions.api.PlayerStatus;
import me.choco.conditions.utils.Messages;

@SuppressWarnings("deprecation")
public class ConditionsCommand implements CommandExecutor{
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		Plugin PC = Bukkit.getPluginManager().getPlugin("PlayerConditions");
		if (commandLabel.equalsIgnoreCase("conditions") || commandLabel.equalsIgnoreCase("condition")){
			if (sender instanceof Player){
				Player player = (Player) sender;
				if (args.length > 0){
					if (args[0].equalsIgnoreCase("version")){
						if (player.hasPermission("conditions.version")){
							player.sendMessage(ChatColor.GOLD + "--------------------------------------------");
							player.sendMessage(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "Version: " + ChatColor.RESET + "" + ChatColor.GRAY + PC.getDescription().getVersion());
							player.sendMessage(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "Developer / Maintainer: " + ChatColor.RESET + "" + ChatColor.GRAY + "2008Choco");
							player.sendMessage(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "Other Popular Plugins: " + ChatColor.RESET + "" + ChatColor.GRAY + "FallRoll, ToolRemains, AlchemicalArrows");
							player.sendMessage(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "Development Page: " + ChatColor.RESET + "" + ChatColor.GRAY + "http://dev.bukkit.org/bukkit-plugins/player-conditions");
							player.sendMessage(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "Report Bugs To: " + ChatColor.RESET + "" + ChatColor.GRAY + "dev.bukkit.org/bukkit-plugins/player-conditions/tickets");
							player.sendMessage(ChatColor.GOLD + "--------------------------------------------");
							return true;
						}//Close if permissions == true
						else{
							Messages.send(player, ChatColor.RED, "You do not have the sufficient permisions for this command");
							return true;
						}//Close if permissions == false
					}//Close if args[0] == version
					if (args[0].equalsIgnoreCase("reload")){
						PC.reloadConfig();
						Messages.send(player, ChatColor.RED, ChatColor.GREEN + "The configuration has been reloaded");
					}
					
					if (args[0].equalsIgnoreCase("bleed") || args[0].equalsIgnoreCase("bleeding")){
						if (player.hasPermission("conditions.bleeding")){
							if (args.length == 3){
								if (args[1].equalsIgnoreCase("force")){
									final Player targetPlayer = Bukkit.getPlayer(args[2]);
									if (Bukkit.getOnlinePlayers().contains(targetPlayer)){
										if (!PlayerStatus.isBleeding(targetPlayer)){
											PlayerStatus.setCondition(targetPlayer, Condition.BLEEDING);
											Messages.send(player, ChatColor.DARK_RED, targetPlayer.getName() + " is now bleeding");
											Messages.send(targetPlayer, ChatColor.DARK_RED, player.getName() + " has forced you to bleed");
											return true;
										}//Close if player is already bleeding
										else{
											Messages.send(player, ChatColor.DARK_RED, targetPlayer.getName() + " is already bleeding! How cruel!");
											return true;
										}//Close if player is NOT already bleeding
									}//Close if player is online
									else{
										Messages.send(player, ChatColor.DARK_RED, args[2] + " is not currently online");
										return true;
									}//Close if player is offline
								}//Close if args[0] == "force"
							}//Close if args.length == 3
							if (args.length == 3){
								if (args[1].equalsIgnoreCase("stop")){
									Player targetPlayer = Bukkit.getPlayer(args[2]);
									if (Bukkit.getOnlinePlayers().contains(targetPlayer)){
										if (PlayerStatus.isBleeding(targetPlayer)){
											PlayerStatus.removeCondition(targetPlayer, Condition.BLEEDING);
											Messages.send(player, ChatColor.DARK_RED, targetPlayer.getName() + " has stopped bleeding");
											Messages.send(targetPlayer, ChatColor.DARK_RED, player.getName() + " has cured your bleeding");
											return true;
										}//Close if player is already bleeding
										else{
											Messages.send(player, ChatColor.DARK_RED, targetPlayer.getName() + " isn't bleeding. Cannot stop");
											return true;
										}//Close if player is NOT already bleeding
									}//Close if player is online
									else{
										Messages.send(player, ChatColor.DARK_RED, args[2] + " is not currently online");
										return true;
									}//Close if player is offline
								}//Close if args[1] == "stop"
							}//Close if args.length == 3
							if (args.length == 1){
								player.sendMessage(ChatColor.WHITE + "------------" + ChatColor.DARK_RED + " Bleeding Commands " + ChatColor.WHITE + "------------");
								player.sendMessage(ChatColor.RED + "/conditions bleed force <player> " + ChatColor.WHITE + "- Force a player to bleed");
								player.sendMessage(ChatColor.RED + "/conditions bleed stop <player> " + ChatColor.WHITE + "- Stop a players bleeding");
								player.sendMessage(ChatColor.WHITE + "----------------------------------------");
								return true;
							}//Close if args.length == 1
						}//Close if permissions == true
						else{
							Messages.send(player, ChatColor.DARK_RED, "You do not have the sufficient permisions for this command");
							return true;
						}//Close if permissions == false
					}//Close if bleed condition is specified
					
					if (args[0].equalsIgnoreCase("infect") || args[0].equalsIgnoreCase("infection")){
						if (player.hasPermission("conditions.infection")){
							if (args.length == 3){
								if (args[1].equalsIgnoreCase("force")){
									final Player targetPlayer = Bukkit.getPlayer(args[2]);
									if (Bukkit.getOnlinePlayers().contains(targetPlayer)){
										if (!PlayerStatus.isInfected(targetPlayer)){
											PlayerStatus.setCondition(targetPlayer, Condition.INFECTION);
											Messages.send(player, ChatColor.DARK_GREEN, targetPlayer.getName() + " is now infected");
											Messages.send(targetPlayer, ChatColor.DARK_GREEN, player.getName() + " has infected you");
											return true;
										}//Close if player is already bleeding
										else{
											Messages.send(player, ChatColor.DARK_GREEN, targetPlayer.getName() + " is already infected! How cruel!");
											return true;
										}//Close if player is NOT already bleeding
									}//Close if player is online
									else{
										Messages.send(player, ChatColor.DARK_GREEN, args[2] + " is not currently online");
										return true;
									}//Close if player is offline
								}//Close if args[0] == "force"
							}//Close if args.length == 3
							if (args.length == 3){
								if (args[1].equalsIgnoreCase("cure")){
									Player targetPlayer = Bukkit.getPlayer(args[2]);
									if (Bukkit.getOnlinePlayers().contains(targetPlayer)){
										if (PlayerStatus.isInfected(targetPlayer)){
											PlayerStatus.removeCondition(targetPlayer, Condition.INFECTION);
											Messages.send(player, ChatColor.DARK_GREEN, targetPlayer.getName() + " has been cured");
											Messages.send(targetPlayer, ChatColor.DARK_GREEN, player.getName() + " has cured your infection");
											return true;
										}//Close if player is already bleeding
										else{
											Messages.send(player, ChatColor.DARK_GREEN, targetPlayer.getName() + " isn't infeced. Cannot cure");
											return true;
										}//Close if player is NOT already bleeding
									}//Close if player is online
									else{
										Messages.send(player, ChatColor.DARK_GREEN, args[2] + " is not currently online");
										return true;
									}//Close if player is offline
								}//Close if args[1] == "cure"
							}//Close if args.length == 3
							if (args.length == 1){
								player.sendMessage(ChatColor.WHITE + "------------" + ChatColor.DARK_GREEN + " Infection Commands " + ChatColor.WHITE + "------------");
								player.sendMessage(ChatColor.GREEN + "/conditions infect force <player> " + ChatColor.WHITE + "- Force a player to become infected");
								player.sendMessage(ChatColor.GREEN + "/conditions infect cure <player> " + ChatColor.WHITE + "- Cure a player from infection");
								player.sendMessage(ChatColor.WHITE + "-----------------------------------------");
								return true;
							}//Close if args.length == 1
						}//Close if permissions == true
						else{
							Messages.send(player, ChatColor.DARK_GREEN, "You do not have the sufficient permisions for this command");
							return true;
						}//Close if permissions == false
					}//Close if infect condition is specified
					
					if (args[0].equalsIgnoreCase("frostbite")){
						if (player.hasPermission("conditions.frostbite")){
							if (args.length == 3){
								if (args[1].equalsIgnoreCase("force")){
									final Player targetPlayer = Bukkit.getPlayer(args[2]);
									if (Bukkit.getOnlinePlayers().contains(targetPlayer)){
										if (!PlayerStatus.isFrostbitten(targetPlayer)){
											PlayerStatus.setCondition(targetPlayer, Condition.FROSTBITE);
											Messages.send(player, ChatColor.AQUA, targetPlayer.getName() + " is now frostbitten");
											Messages.send(targetPlayer, ChatColor.AQUA, player.getName() + " has forcibly frostbitten you");
											return true;
										}//Close if player is already frostbitten
										else{
											Messages.send(player, ChatColor.AQUA, targetPlayer.getName() + " is already frostbitten! How cruel!");
											return true;
										}//Close if player is NOT already frostbitten
									}//Close if player is online
									else{
										Messages.send(player, ChatColor.AQUA, args[2] + " is not currently online");
										return true;
									}//Close if player is offline
								}//Close if args[0] == "force"
							}//Close if args.length == 3
							if (args.length == 3){
								if (args[1].equalsIgnoreCase("cure")){
									Player targetPlayer = Bukkit.getPlayer(args[2]);
									if (Bukkit.getOnlinePlayers().contains(targetPlayer)){
										if (PlayerStatus.isFrostbitten(targetPlayer)){
											PlayerStatus.removeCondition(targetPlayer, Condition.FROSTBITE);
											Messages.send(player, ChatColor.AQUA, targetPlayer.getName() + " has been cured");
											Messages.send(targetPlayer, ChatColor.AQUA, player.getName() + " has cured your frostbite");
											return true;
										}//Close if player is already frostbitten
										else{
											Messages.send(player, ChatColor.AQUA, targetPlayer.getName() + " isn't frostbitten. Cannot cure");
											return true;
										}//Close if player is NOT already frostbitten
									}//Close if player is online
									else{
										Messages.send(player, ChatColor.AQUA, args[2] + " is not currently online");
										return true;
									}//Close if player is offline
								}//Close if args[1] == "cure"
							}//Close if args.length == 3
							if (args.length == 1){
								player.sendMessage(ChatColor.WHITE + "------------" + ChatColor.AQUA + " Frostbite Commands " + ChatColor.WHITE + "------------");
								player.sendMessage(ChatColor.AQUA + "/conditions frostbite force <player> " + ChatColor.WHITE + "- Force a player to become frostbitten");
								player.sendMessage(ChatColor.AQUA + "/conditions frostbite cure <player> " + ChatColor.WHITE + "- Cure a player from frostbite");
								player.sendMessage(ChatColor.WHITE + "-----------------------------------------");
								return true;
							}//Close if args.length == 1
						}//Close if permissions == true
						else{
							Messages.send(player, ChatColor.AQUA, "You do not have the sufficient permisions for this command");
							return true;
						}//Close if permissions == false
					}//Close if frostbite condition is specified
					
					if (args[0].equalsIgnoreCase("mystophobia")){
						if (player.hasPermission("conditions.mystophobia")){
							if (args.length == 3){
								if (args[1].equalsIgnoreCase("force")){
									final Player targetPlayer = Bukkit.getPlayer(args[2]);
									if (Bukkit.getOnlinePlayers().contains(targetPlayer)){
										if (!PlayerStatus.isMystified(targetPlayer)){
											PlayerStatus.setCondition(player, Condition.MYSTOPHOBIA);
											Messages.send(player, ChatColor.DARK_GRAY, targetPlayer.getName() + " is now mystified");
											Messages.send(targetPlayer, ChatColor.DARK_GRAY, player.getName() + " has mystified you");
											return true;
										}//Close if player is NOT already mystified
										else{
											Messages.send(player, ChatColor.DARK_GRAY, targetPlayer.getName() + " is already mystified! How cruel!");
											return true;
										}//Close if player is already mystified
									}//Close if player is online
									else{
										Messages.send(player, ChatColor.DARK_GRAY, args[2] + " is not currently online");
										return true;
									}//Close if player is offline
								}//Close if args[0] == "force"
							}//Close if args.length == 3
							if (args.length == 3){
								if (args[1].equalsIgnoreCase("cure")){
									Player targetPlayer = Bukkit.getPlayer(args[2]);
									if (Bukkit.getOnlinePlayers().contains(targetPlayer)){
										if (PlayerStatus.isMystified(targetPlayer)){
											PlayerStatus.removeCondition(targetPlayer, Condition.MYSTOPHOBIA);
											Messages.send(player, ChatColor.DARK_GRAY, targetPlayer.getName() + " has been cured");
											Messages.send(targetPlayer, ChatColor.DARK_GRAY, player.getName() + " has cured your mystification");
											return true;
										}//Close if player is already mystified
										else{
											Messages.send(player, ChatColor.DARK_GRAY, targetPlayer.getName() + " isn't mystified. Cannot cure");
											return true;
										}//Close if player is NOT already mystified
									}//Close if player is online
									else{
										Messages.send(player, ChatColor.DARK_GRAY, args[2] + " is not currently online");
										return true;
									}//Close if player is offline
								}//Close if args[1] == "cure"
							}//Close if args.length == 3
							if (args.length == 1){
								player.sendMessage(ChatColor.WHITE + "------------" + ChatColor.DARK_GRAY + " Mystophobia Commands " + ChatColor.WHITE + "------------");
								player.sendMessage(ChatColor.GRAY + "/conditions mystophobia force <player> " + ChatColor.WHITE + "- Force a player to become mystified");
								player.sendMessage(ChatColor.GRAY + "/conditions mystophobia cure <player> " + ChatColor.WHITE + "- Cure a player from mystification");
								player.sendMessage(ChatColor.WHITE + "-----------------------------------------");
								return true;
							}//Close if args.length == 1
						}//Close if permissions == true
						else{
							Messages.send(player, ChatColor.DARK_GRAY, "You do not have the sufficient permisions for this command");
							return true;
						}//Close if permissions == false
					}//Close if mystophobia condition is specified
					
					if (args[0].equalsIgnoreCase("arachnophobia") || args[0].equalsIgnoreCase("spider") || args[0].equalsIgnoreCase("spiders")){
						if (player.hasPermission("conditions.arachnophobia")){
							if (args.length == 3){
								if (args[1].equalsIgnoreCase("force")){
									final Player targetPlayer = Bukkit.getPlayer(args[2]);
									if (Bukkit.getOnlinePlayers().contains(targetPlayer)){
										if (!PlayerStatus.isArachnophobic(targetPlayer)){
											PlayerStatus.setCondition(targetPlayer, Condition.ARACHNOPHOBIA);
											Messages.send(player, ChatColor.DARK_GRAY, targetPlayer.getName() + " is now arachnophobic");
											Messages.send(targetPlayer, ChatColor.DARK_GRAY, player.getName() + " has forced you to become arachnophobic");
											return true;
										}//Close if player is already bleeding
										else{
											Messages.send(player, ChatColor.DARK_GRAY, targetPlayer.getName() + " is already arachnophobic! How cruel!");
											return true;
										}//Close if player is NOT already bleeding
									}//Close if player is online
									else{
										Messages.send(player, ChatColor.DARK_GRAY, args[2] + " is not currently online");
										return true;
									}//Close if player is offline
								}//Close if args[0] == "force"
							}//Close if args.length == 3
							if (args.length == 3){
								if (args[1].equalsIgnoreCase("cure")){
									Player targetPlayer = Bukkit.getPlayer(args[2]);
									if (Bukkit.getOnlinePlayers().contains(targetPlayer)){
										if (PlayerStatus.isArachnophobic(targetPlayer)){
											PlayerStatus.removeCondition(targetPlayer, Condition.ARACHNOPHOBIA);
											Messages.send(player, ChatColor.DARK_GRAY, targetPlayer.getName() + " is no longer arachnophobic");
											Messages.send(targetPlayer, ChatColor.DARK_GRAY, player.getName() + " has cured your arachnophobia");
											return true;
										}//Close if player is already bleeding
										else{
											Messages.send(player, ChatColor.DARK_GRAY, targetPlayer.getName() + " isn't arachnophobic. Cannot cure");
											return true;
										}//Close if player is NOT already bleeding
									}//Close if player is online
									else{
										Messages.send(player, ChatColor.DARK_GRAY, args[2] + " is not currently online");
										return true;
									}//Close if player is offline
								}//Close if args[1] == "stop"
							}//Close if args.length == 3
							if (args.length == 1){
								player.sendMessage(ChatColor.WHITE + "------------" + ChatColor.GRAY + " Ararchnophobia Commands " + ChatColor.WHITE + "------------");
								player.sendMessage(ChatColor.DARK_GRAY + "/conditions arachnophobia force <player> " + ChatColor.WHITE + "- Force a player to be arachnophobic");
								player.sendMessage(ChatColor.DARK_GRAY + "/conditions arachnophobia cure <player> " + ChatColor.WHITE + "- Settle a player's arachnophobia");
								player.sendMessage(ChatColor.WHITE + "----------------------------------------");
								return true;
							}//Close if args.length == 1
						}//Close if permissions == true
						else{
							Messages.send(player, ChatColor.DARK_GRAY, "You do not have the sufficient permisions for this command");
							return true;
						}//Close if permissions == false
					}//Close if archnophobia condition is specified
					
					if (args[0].equalsIgnoreCase("malaria")){
						if (player.hasPermission("conditions.malaria")){
							if (args.length == 3){
								if (args[1].equalsIgnoreCase("force")){
									final Player targetPlayer = Bukkit.getPlayer(args[2]);
									if (Bukkit.getOnlinePlayers().contains(targetPlayer)){
										if (!PlayerStatus.hasMalaria(targetPlayer)){
											PlayerStatus.setCondition(targetPlayer, Condition.MALARIA);
											Messages.send(player, ChatColor.DARK_PURPLE, targetPlayer.getName() + " now has malaria");
											Messages.send(targetPlayer, ChatColor.DARK_PURPLE, player.getName() + " has contracted you with malaria");
											return true;
										}//Close if player is already bleeding
										else{
											Messages.send(player, ChatColor.DARK_PURPLE, targetPlayer.getName() + " already has malaria! How cruel!");
											return true;
										}//Close if player is NOT already bleeding
									}//Close if player is online
									else{
										Messages.send(player, ChatColor.DARK_PURPLE, args[2] + " is not currently online");
										return true;
									}//Close if player is offline
								}//Close if args[0] == "force"
							}//Close if args.length == 3
							if (args.length == 3){
								if (args[1].equalsIgnoreCase("cure")){
									Player targetPlayer = Bukkit.getPlayer(args[2]);
									if (Bukkit.getOnlinePlayers().contains(targetPlayer)){
										if (PlayerStatus.hasMalaria(targetPlayer)){
											PlayerStatus.removeCondition(targetPlayer, Condition.MALARIA);
											Messages.send(player, ChatColor.DARK_PURPLE, targetPlayer.getName() + " no longer has malaria");
											Messages.send(targetPlayer, ChatColor.DARK_PURPLE, player.getName() + " has cured your malaria");
											return true;
										}//Close if player is already bleeding
										else{
											Messages.send(player, ChatColor.DARK_PURPLE, targetPlayer.getName() + " doesn't have malaria. Cannot cure");
											return true;
										}//Close if player is NOT already bleeding
									}//Close if player is online
									else{
										Messages.send(player, ChatColor.DARK_PURPLE, args[2] + " is not currently online");
										return true;
									}//Close if player is offline
								}//Close if args[1] == "stop"
							}//Close if args.length == 3
							if (args.length == 1){
								player.sendMessage(ChatColor.WHITE + "------------" + ChatColor.DARK_PURPLE + " Malaria Commands " + ChatColor.WHITE + "------------");
								player.sendMessage(ChatColor.DARK_PURPLE + "/conditions malaria force <player> " + ChatColor.WHITE + "- Force a player to contract malaria");
								player.sendMessage(ChatColor.DARK_PURPLE + "/conditions malaria cure <player> " + ChatColor.WHITE + "- Cure a player from malaria");
								player.sendMessage(ChatColor.WHITE + "----------------------------------------");
								return true;
							}//Close if args.length == 1
						}//Close if permissions == true
						else{
							Messages.send(player, ChatColor.DARK_PURPLE, "You do not have the sufficient permisions for this command");
							return true;
						}//Close if permissions == false
					}//Close if malaria condition is specified
					
					else{
						Messages.send(player, ChatColor.RED, "Invalid arguments");
						return true;
					}//Close if args are invalid
				}//Close if args.length == 1
				if (args.length == 0){
					player.sendMessage(ChatColor.WHITE + "------------" + ChatColor.DARK_RED + " General Commands " + ChatColor.WHITE + "------------");
					player.sendMessage(ChatColor.GRAY + "/conditions version " + ChatColor.WHITE + "- Shows the current version of PlayerConditions");
					player.sendMessage(ChatColor.RED + "/conditions bleed " + ChatColor.WHITE + "- View all bleeding condition sub-commands");
					player.sendMessage(ChatColor.GREEN + "/conditions infect " + ChatColor.WHITE + "- View all infection condition sub-commands");
					player.sendMessage(ChatColor.AQUA + "/conditions frostbite " + ChatColor.WHITE + "- View all frostbite condition sub-commands");
					player.sendMessage(ChatColor.DARK_GRAY + "/conditions mystophobia " + ChatColor.WHITE + "- View all mystophobia condition sub-commands");
					player.sendMessage(ChatColor.GRAY + "/conditions arachnophobia " + ChatColor.WHITE + "- View all arachnophobic conditions");
					player.sendMessage(ChatColor.DARK_PURPLE + "/conditions malaria " + ChatColor.WHITE + "- View all malaria condition sub-commands");
					player.sendMessage(ChatColor.WHITE + "----------------------------------------");
					return true;
				}//Close if args.length == 0
			}//Close if sender == player
		}//Close conditions command
		return false;
	}//Close onCommand method
}//Close class