package me.choco.conditions.api;

public enum Condition {
	BLEEDING(),
	INFECTION(),
	VACCINATED(),
	FOOD_POISONING(),
	FROSTBITE(),
	MYSTOPHOBIA(),
	HUNGRY(),
	ARACHNOPHOBIA(),
	MALARIA(),
	SLIMED();
}//Close enumeration