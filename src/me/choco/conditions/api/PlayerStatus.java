package me.choco.conditions.api;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;

public class PlayerStatus {
	
	static Plugin PC = Bukkit.getPluginManager().getPlugin("PlayerConditions");
	
	public static boolean isBleeding(Player player){
		if (player.hasMetadata("Bleeding"))
			return true;
		return false;
	}
	
	public static boolean isInfected(Player player){
		if (player.hasMetadata("Infected"))
			return true;
		return false;
	}
	
	public static boolean isVaccinated(Player player){
		if (player.hasMetadata("Vaccinated"))
			return true;
		return false;
	}
	
	public static boolean isFoodPoisoned(Player player){
		if (player.hasMetadata("FoodPoisoned"))
			return true;
		return false;
	}
	
	public static boolean isFrostbitten(Player player){
		if (player.hasMetadata("Frostbitten"))
			return true;
		return false;
	}
	
	public static boolean isMystified(Player player){
		if (player.hasMetadata("Mystified"))
			return true;
		return false;
	}
	
	public static boolean isHungry(Player player){
		if (player.hasMetadata("Hungry"))
			return true;
		return false;
	}
	
	public static boolean isArachnophobic(Player player){
		if (player.hasMetadata("Arachnophobic"))
			return true;
		return false;
	}
	
	public static boolean hasMalaria(Player player){
		if (player.hasMetadata("Malaria"))
			return true;
		return false;
	}
	
	public static void setCondition(Player player, Condition condition){
		switch(condition){
		case ARACHNOPHOBIA:
			player.setMetadata("Arachnophobic", new FixedMetadataValue(PC, player));
			break;
		case BLEEDING:
			player.setMetadata("Bleeding", new FixedMetadataValue(PC, player));
			break;
		case FOOD_POISONING:
			player.setMetadata("FoodPoisoned", new FixedMetadataValue(PC, player));
			break;
		case FROSTBITE:
			player.setMetadata("Frostbitten", new FixedMetadataValue(PC, player));
			break;
		case HUNGRY:
			player.setMetadata("Hungry", new FixedMetadataValue(PC, player));
			break;
		case INFECTION:
			player.setMetadata("Infected", new FixedMetadataValue(PC, player));
			break;
		case MALARIA:
			player.setMetadata("Malaria", new FixedMetadataValue(PC, player));
			break;
		case MYSTOPHOBIA:
			player.setMetadata("Mystified", new FixedMetadataValue(PC, player));
			break;
		case SLIMED:
			player.setMetadata("Slimed", new FixedMetadataValue(PC, player));
			break;
		case VACCINATED:
			player.setMetadata("Vaccinated", new FixedMetadataValue(PC, player));
			break;
		}
	}
	
	public static void removeCondition(Player player, Condition condition){
		switch(condition){
		case ARACHNOPHOBIA:
			if (player.hasMetadata("Arachnophoic"))
				player.removeMetadata("Arachnophobic", PC);
			break;
		case BLEEDING:
			if (player.hasMetadata("Bleeding"))
				player.removeMetadata("Bleeding", PC);
			break;
		case FOOD_POISONING:
			if (player.hasMetadata("FoodPoisoned"))
				player.removeMetadata("FoodPoisoned", PC);
			break;
		case FROSTBITE:
			if (player.hasMetadata("Frostbitten"))
				player.removeMetadata("Frostbitten", PC);
			break;
		case HUNGRY:
			if (player.hasMetadata("Hungry"))
				player.removeMetadata("Hungry", PC);
			break;
		case INFECTION:
			if (player.hasMetadata("Infected"))
				player.removeMetadata("Infected", PC);
			break;
		case MALARIA:
			if (player.hasMetadata("Malaria"))
				player.removeMetadata("Malaria", PC);
			break;
		case MYSTOPHOBIA:
			if (player.hasMetadata("Mystified"))
				player.removeMetadata("Mystified", PC);
			break;
		case SLIMED:
			if (player.hasMetadata("Slimed"))
				player.removeMetadata("Slimed", PC);
			break;
		case VACCINATED:
			if (player.hasMetadata("Vaccinated"))
				player.removeMetadata("Vaccinated", PC);
			break;
		}
	}
}//Close class