<center><img src="http://i.imgur.com/3N9Aoss.png", width=150, height=150></center>

# <center> Changelog </center> #
- - - - - - - - - -

This is where you will find the official changelog of all releases of*PlayerConditions &copy;*. This will be updated periodically in order to keep track of all changes related to this document. This is a large database of all changelogs that have ever been published to the official BukkitDev page.

The official BukkitDev page can be found by [clicking here](http://dev.bukkit.org/bukkit-plugins/player-conditions)

## Official 2.0 Releases ##
- - - - - - - - - -

### Version 2.0.1 ###
* Potentially fixed NullPointerException related to Frostbite
* Added a /conditions reload command due to my inability to realize that I removed the /pcconfig reload command
* Fixed the inability to receive Frostbite in a randomization
* Cleaned and condensed some code related to Frostbite

### Version 2.0.0 ###
* Added a Startup class to handle startup methods (and removed startup package to save memory)
* Removed the /pcconfig command
* Removed the UseBandageNoHeal message config option (unused)
* Removed Console message if metrics are disabled
* Added a changelog.md file to log all changelogs ever logged in the history of PlayerConditions
* Added an e.printStackTrace() method if metrics could not enable
* Modified the imports and methods to expand the Startup class rather than the deleted classes
* Fixed multiple declaration of the plugin to save memory
* Changed checking direct EntityType, to instanceof
* Declared ParticleData in the beginning of most classes
* Blood effects on all mobs damaging the player rather than just specific mobs
* Completely rewrote the entire recipe handling class
* Added simplifications and declarations in the code to read it better
* Added internal methods to simplify and speed up code processes
|--> itemEquals(player, itemType, itemName) method in replacement to check item MetaData
|--> removeCurrentItem(player) method to remove items from players inventories (cleans code)
|--> bandageHandler(player, healthIncrease, isBleeding) method to handle the bandage click event
|--> createItem(Material itemMaterial, String itemName, ArrayList<String> itemLore) method to create modified cure items
|--> setHungerSlowness() method to simplify the addition of slowness when hungry
* Added internal message implementations to clean the message sending process
* Fixed regular eyes of ender not being able to be thrown
* Removed my specialized join message (#SadFace)
* Added a pcColour parameter in the "send" method to allow for the changing colours of the "PlayerConditions>" tag before messages
* Removed multiple declarations of Random's and created a static reference in the beginning of the class
* Compiled all startup methods into one class to save memory
* Renamed all generic "enable" and "load" methods to "registerEvents", "registerCommands", and "loadItems"
* Changed the way all conditions are handled (Handled through metadata rather than arraylists)
* Started the process of creating a basic API (may be ready for next version)
* Added a Condition Enumeration class to list all possible conditions
* Added a PlayerStatus class
|--> isBleeding(), isInfected(), isVaccinated, isFoodPoisoned(), isFrostbitten(), isMystified(), isHungry(), isArachnophobic(), and hasMalaria() boolean methods to determine whether a player has a specific condition
|--> setCondition() method to add a condition to a player
|--> removeCondition() method to remove a condition from a player
* Removed all ArrayList initiations in the main class
* Fixed some grammar mistakes in the conditions command (Oops. Still tons of work to do on this command)

## Official Pre-2.0 Releases ##
- - - - - - - - - -

### Version 1.7.3 ###
* Changed the food poisoning sound to be more sick-like rather than zombie-like
* Fixed food poisoning occurring 100% of the time regardless of the setting
* Fixed malaria being impossible to obtain
* Just for the sake of silliness, added a join message for players that are specified in the code (Really, it just says hi when I join :D)
* When I join the server, it'll just let everyone know that I'm the developer. (Leave your server IP so we can list it on the servers page)

### Version 1.7.2 ###
* When damaged by creepers, shell shock will occur as well
* Added a chance of bleeding if you take damage on a cactus (New config option to disable it)
* Added a sickening groan when you become food poisoned (to simulate vomiting?)
* A chance of bleeding from fall damage now only occurs when 4 hearts of damage or more is taken
* Introducing, semi-conditions! These are temporary conditions that will last for a short burst of time. Most will be positive
 ### When eating an apple, you will receive regeneration II for 3 seconds

### Version 1.7.1 ###
* Fixed the malaria cure throwing an eye of ender when right clicked
* Fixed being impossible to receive arachnophobia from spiders
* Changed the zombification noise when infected to be MUCH MUCH MUCH creepier
* Added a sound on death when infected. A deep, mortifying groan will be played
* Chicken Noodle Soup and Antibiotics are now shapeless recipes
* Changed the dispertion of particles when consuming a cure to be more spread out
* Temporarily took out the "API version" information in the PlayerConditions version command

### Version 1.7.0.1 ###
* Fixed parsing error in the configuration file, causing many errors in the console upon startup

### Version 1.7.0 ###
* Revamped the config to be much more legible without comments. (Information on config options can be found on the main page)
* Added configurable messages in the configuration file (these are excluded from the /pcconfig command) (CLOSES TICKET #3)
* Re-designed the look of the /conditions version, information command. This may be final
* Removed arachnophobia being cleared after death. Didn't make any sense to lose a non-hostile ability after dying
* Fixed errors in the console whenever a player died
* Fixed an error appearing when eating regular mushroom stew
* Fixed re-definitions of self-references being called on multiple occasions
* Added rotten flesh to the list of items that will give you food poisoning

### Version 1.6.2 ###
* Added new minor condition: Concussion.
* When hit by a falling block, you will get slowness II for 10 seconds
* Added new minor condition: Shell Shock.
* When damaged by an explosion, you will receive slowness II for 10 seconds

### Version 1.6.1 ###
* Taking fall damage will now have a possibility of making you bleed
* Added a new configuration option under the Bleeding section to disable fall damage bleeding

### Version 1.6.0 ###
* Fixed players having their conditions, even after death. Now, all conditions will be cleared after death
* Changed the dark_gray "Arachnophobia" command to light gray
* Fixed the conditions command displaying the wrong titles for sub-commands
* Combined 10 class files for randomizations into 1 single class file. Should decrease file size IMMENSELY (May decrease startup time)
* Added a new condition, MALARIA! Take caution when hanging in swamp. Mosquitos might bite ya!
* You will slowly take damage over time with a slight bit of confusion
* Added a cure for malaria, "Bug Spray". You can view the crafting recipe on the main page
* Added a conditions sub-command for malaria (/conditions malaria)

### Version 1.5.3 ###
* Grouped all click events into one class (Should decrease file size significantly)
* Fixed an error when clicking on a poppy, ghast tear, spider web, or a piece of paper that is not named (Reported by xDizasterCYx)
* Added a death message for when a player dies of food poisoning

### Version 1.5.2 ###
* Compiled to support CraftBukkit versions 1.8.0 - 1.8.7
* The recipe for antibiotics now takes an empty bowl rather than mushroom stew

### Version 1.5.1 ###
* Removed potion effects for arachnophobic players every 7 seconds
* Changed arachnophobia to deal 1 heart of extra damage when hit by a spider
* Fixed the cure for arachnophobia not functioning when right clicking
* Added arachnophobic commands. Don't worry, if you can't spell arachnophobia, "spider" or "spiders" works too :P (Permission: conditions.arachnophobia)
* Fixed the possibility of being arachnophobic while you're already arachnophobic
* Randomized effects continue, alongside taking 1 heart of extra damage from endermen while mystified

### Version 1.5.0 ###
* Added long-awaited, Plugin Metrics. Player Conditions will not anonymously send server information to http://mcstats.org/plugin/playerconditions
* If you do not wish to take part in the Plugin Metrics, there is a configuration option inside the PlayerCondition config (DEFAULT: TRUE)
* Fixed the slimed condition not being given by slimes
* Fixed the /pcconfig list command not displaying the SlimedChance configuration option
* Added slime particle effects when a slime hits you, and blood particles when a spider hits you
* Added arachnophobia. Be careful if you're hit by a spider, you might get scared ;) (No config options yet :( Sorry. Will do for 1.5.1)
* Added a cure for arachnophobia. Put 9 string in a crafting table... yes... string

### Version 1.4.3 ###
* Compiled Player Conditions to be compatible with versions 1.8.1 - 1.8.6
* Fixed potions of slowness being useless due to slowness being cleared after 1 second
* Added a "Slimed" condition. If hit by a slime, there's a 15% chance you'll become slowed
* 15% is DEFAULT and can be changed in the configuration file. Please delete and reload the configuration file

### Version 1.4.2 ###
* Added customizable chances for bleeding, infections, confusion, food poisoning, and mystophobia (Can be found in config)
* Added colours to the /pcconfig list command. DARK_AQUA for integers, LIGHT_PURPLE for booleans
* Increased chance of zombie infection by 5% (default), and the chance of mystophobia to 10% (default)
* Potential fix for slowness when hungry after having a saturation effect
* Fixed a safeguard for using /pcconfig from the console. Can't do it :( Sorry
* Cleaned up the main startup method. Should faster to start up the plugin
* Removed the shutdown message / config safe as it messed up the configuration file. Unfortunately

### Version 1.4.1 ###
* Re-organized a couple of the startup information messages, and added a shutdown information message
* Added an onDisable function to save the configuration file. Just incase changes were made and the server restarts. It will save those changes

### Version 1.4.0 ###
* Added a new /pcconfig command! Change configuration options in-game without having to reload your server!
* You can list all configuration options available and their current values with /pcconfig list
* Set/change a config option with the /pcconfig set <option> <value>
* Added 3 new permission nodes for these commands. conditions.config.* will allow you to access the entire /pcconfig command
* conditions.config.list will allow you to use /pcconfig list, and conditions.config.set will allow you to set a config option
* Changed the /conditions reload command to /pcconfig reload. Made more sense. New permission node (conditions.config.reload)
* Added an extra information log on startup in the console for enabling events
* Lots and lots of bug fixes:
|--> Fixed grammatical errors in strings
|--> Fixed the Flower of Purity not working to cure Mystophobia
|--> Fixed the lore not displaying on the Flower of Purity
|--> Fixed slowness potency not increasing when at a lower hunger level
|--> Fixed eating raw items not counting as a fix for hunger slowness
|--> Fixed particle effects not showing when clicking on a cure

### Version 1.3.2.1 ###
* Fixed a fatal startup due to an extra space in a crafting recipe (Heh. Oops)

### Version 1.3.2 ###
* Added a minor condition of Mystophobia. If damaged by an enderman, there is a 1% chance of being mystified
* When mystified, you will receive random effects of blindness, confusion, and slowness every x seconds (set in configuration)
* Added, Flower of Purity, and a recipe (found on main page) to cure the mystophobia
* Added a section of sub-commands for mystophobia in the /conditions command. Permission required: conditions.mystophobia
* Changed the description of the plugin to reflect the current intention (Only viewable for MCMyAdmin users)

### Version 1.3.1 ###
* Added logger information in the console on startup to inform server owners that everything is starting properly
* Added 3 death messages for Frostbite (I forgot :P)
* Added particle effects when you use one of the 4 cures/bandages

### Version 1.3.0 ###
* Added a blood splash upon death of a player. Larger, more spread out, and bloodier
* Fixed the antibiotics not removing the effects for food poisoning
* Fixed still being slow for a bit while hungry, and eating past 3 bars of hunger
* Fixed the slowness effect being removed every so often when hungry
* Added a frostbite condition. If you're in a cold biome too long, you're susceptible to frostbite
* Frostbite chances can be prevented by wearing leather armour. If no leather armour is worn, it's a 1% chance of receiving it
* Added a recipe for "Chicken Noodle Soup" which can be used to cure frostbite
* Added a safeguard for eating Chicken Noodle Soup or Antibiotics when you don't need it
* Added a new set of commands for frostbite. /conditions frostbite force <player>, and /conditions frostbite cure <player>
* Added long-awaited separated permission nodes for each condition. View main page for information on permission nodes
* Fixed grammatical errors in a lot of my messages. Heh... oops
* Added 2 new configuration options for Frostbite. Please reload your configuration file

### Version 1.2.3 ###
* Fixed a fatal error on start up due to a slip-up in the crafting recipe for antibiotics
* Fixed Vertigo Condition only checking the specified block, rather than everything higher than it
* Fixed confusion effects lasting for 6/10th's of a second
* Fixed Food Poisoning effects lasting for 3 seconds rather than 30 seconds
* Fixed the /conditions version command returning as if there are invalid arguments
* Added raw Mutton, raw Rabbit, and raw Salmon into the list of items that can cause food poisoning
* Changed the percent chance that food poisoning can occur, to 25%
* Added a message for when the vaccination wears off
* Changed the default height for Vertigo to 150, rather than the previous 100. Too low
* Fixed the zombie infection not inflicting a 40% chance confusion every x seconds.

### Version 1.2.2 ###
* Added antibiotics. A new crafting recipe (can be found on the main page)
* If a player consumes the antibiotics, they will be prevented from food poisoning for the next 10 minutes
* If antibiotics are consumed while you are food poisoned, it will cure it
* Added hunger effects. When a player is at a lower hunger level, they will get progressively slower
* All hunger effects will commence when a player has 3 hunger bars left
* This effect is not fatal, however the slowness in combat could be fatal if you're running away at low health
* Changed the 20 second poison effect in food poisoning to Hunger for 30 seconds
* Decreased the 30 second confusion effect in food poisoning to 15 seconds

### Version 1.2.1 ###
* Changed timing of Vertigo sickness, so it's not a 100% chance as soon as you get over the specified y coordinate
* Added a new condition, Thalassophobia. If submerged in water for too long, you slowly start to move slower and slower as exhaustion increases
* The less air you have, the slower you will become and the harder it will be to swim (no config options as it's passive and will not kill players)
* To recover from thalassophobia, just un-submerge from the water, and your slowness will clear over time
* Added a zombified groan when players are infected

### Version 1.2.0 ###
* Added a version sub-command (/conditions version)
* Changed the zombie spawn upon death if infected. The zombie will now wear the armour you had equipped, and the item you were wielding
* Added a new condition, Vertigo, which can be disabled in the configuration file
* If a player is above the specifed Y coordinate, they are susceptible to Vertigo (nausea)
* This new condition is not fatal, however will be very disorienting to the player and may cause them to move around in strange ways
* Added a new condition, Food Poisoning, which can also be disabled in the configuration file
* If a player eats a raw food, there is a 20% chance that they can become food poisoned
* This condition is also not fatal, however low doses of poison, confusion, and slowness will occur
(There are no cures for Vertigo or Food Poisoning. They will subside over time)

### Version 1.1.2 ###
* Set the events in individual classes and packages. Should be faster response to crafting recipes / right clicking items
* Added varying death messages for falling to the infected, and bleeding out
* Separated many loops into different classes to further precise the accuracy of periodical damage, and lessen the memory usage on servers
* General bug fixes, stability improvements, and decrease in memory usage

### Version 1.1.1 ###
* Separated the commands in a commandExecutor class
* Changed all "Bleeding>" messages to display "Condition" or "Conditions" instead
* Conditions is now the main command instead of "/bleed" to easily individualize all future 
* Changed the structure of all the commands (view main page for information)
* Fixed a few grammatical errors, and argument errors in the commands

### Version 1.1.0 ###
* Separated many randomizations into separate classes. Will only be called when necessary
* Added a zombie infection! If hit by a zombie, there is a 1% chance that you can become infected
* If you are infected, over time you will take damage (much like bleeding). This can be disabled in the config
* While infected, there is a 40% chance that every time you are injured you will become briefly nauseated. This can also be disabled in the config
* Added custom death messages for bleeding to death, and for dying while infected
* If infected upon death, a zombie will spawn on your death location
* Added "Infection Cure" item. If right clicked, you will be cured from the infection
* Added a new crafting recipe. "Infection Cure". Check main page for recipe
* Added temporary placeholder blood particle effects when you are hit by a zombie (will change in the future)
* Changed trajectory of blood particle effects to disperse more
* Revamped the look of the configuration file
* Added 4 new configuration options. InfectionsInflictDamage, InfectionPeriodicalConfusion, InfectionDamageDelay, and InfectionDamage